(ns tcp-socket-chat-server.client
  (:require [clojure.tools.logging :as log])
  (:import (java.net InetSocketAddress)
           (java.nio.channels SocketChannel Selector SelectionKey)
           (java.util.concurrent Executors)
           (java.io BufferedReader InputStreamReader PrintWriter)
           (java.nio ByteBuffer)))
(def EXECUTOR (Executors/newFixedThreadPool 2))
(def TIMEOUT 1000)
(def ^:dynamic SELECTOR (Selector/open))

(defn print-message [^SelectionKey key]
  (let [buff (ByteBuffer/allocate 1024)
        readed (.read (.channel key) buff)]
    (when (and (not= readed -1) (not= readed 0))
      (let [array (.array buff)
            readedB (byte-array readed)]
        (System/arraycopy array 0 readedB 0 readed)
        (log/info "from server:" (.toString (.getRemoteAddress (.channel key))) " message:" (String. readedB))
        )
      )
    )
  )

(defn read-input [socketCh]
  (.select SELECTOR TIMEOUT)
  (let [selectedKeys (.selectedKeys SELECTOR)
        iterator (.iterator selectedKeys)]
    (while (.hasNext iterator)
      (print-message (.next iterator))
      (.remove iterator)
      ))
  (recur socketCh)
  )

(defn- init-output-task [console, socket]
  (reify Runnable (run [_] ((while true (try
                                          (let [message (.readLine console)
                                                buffer (ByteBuffer/wrap (.getBytes message))]
                                            (log/info "from client:" message)
                                            (.write socket buffer))
                                          (catch Exception e (log/error "Failed to process message to server" e))
                                          )))))
  )

(defn- init-input-task [socket]
  (reify Runnable (run [_] ((while true (try
                                          (log/info "Reading from socket")
                                          (read-input socket)
                                          (catch Exception e (.printStackTrace e))
                                          )))))
  )


(defn -main
  "I don't do a whole lot."
  [& args]
  (if-let [host (first args)]
    (if-let [port (second args)]
      (let [inetAddr (InetSocketAddress. host (Integer/parseInt port))
            socket (SocketChannel/open inetAddr)
            inu (BufferedReader. (InputStreamReader. System/in))]
        (log/info "Connected to server " (.getRemoteAddress socket))
        (.configureBlocking socket false)
        (.register socket SELECTOR SelectionKey/OP_WRITE)
        ;Launch two threads one for output, one for input
        (.submit EXECUTOR (init-input-task socket))
        (.submit EXECUTOR (init-output-task inu socket ))
        )
      )
    )
  )