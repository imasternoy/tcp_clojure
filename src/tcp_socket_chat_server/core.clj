(ns tcp-socket-chat-server.core
  (:require [clojure.tools.logging :as log])
  (:gen-class)
  (:import (java.nio.channels ServerSocketChannel Selector SelectionKey)
           (java.net InetSocketAddress)
           (java.nio ByteBuffer)
           (java.util.concurrent Executors)))

(def TIMEOUT 1000)
(def EVENT_LOOP_THREADS 3)

(def ^:dynamic USERS (atom []))
(def ^:dynamic SELECTORS [])                                ;List({Selector -> SingleThreadPoolExecutor})

(defn- accept-client [serverCh]
  (let [socketCh (.accept serverCh)]
    (.configureBlocking socketCh false)
    (log/info "Client connected:" (.toString (.getRemoteSocketAddress (.socket socketCh))))
    (let [selector (:selector (nth SELECTORS (rand-int (count SELECTORS))))]
      (.register socketCh selector SelectionKey/OP_READ)
      (swap! USERS conj socketCh)
      ))
  )

(defn broadcast-msg [msg]
  (log/info "Broadcasting message:" (String. msg) "to clients" (count @USERS))
  (doall (for [serverCh @USERS]
           (.write serverCh (ByteBuffer/wrap msg))))
  )

(defn remove_client [^SelectionKey key]
  (log/info "Client:" (.toString (.getRemoteAddress (.channel key))) "disconnected")
  (let [channel (.channel key)]
    (swap! USERS (fn [s] (remove #{channel} s)))
    (.close channel)
    (.cancel key)
    )
  )

(defn process-message [key]
  (let [buff (ByteBuffer/allocate 1024)
        serverCh (.channel key)
        readed (.read serverCh buff)]
    (if (not= readed -1)
      (let [array (.array buff)
            readedB (byte-array readed)]
        (System/arraycopy array 0 readedB 0 readed)
        (broadcast-msg readedB)
        (log/info "From client:" (.toString (.getRemoteAddress (.channel key))) " message:" (String. readedB))
        )
      (remove_client key)
      )
    )
  )

(defn- accept-key [^SelectionKey key]
    (when (.isAcceptable key)
      (accept-client (.channel key)))
    (when (.isReadable key)
      (process-message key )
      )
    )

(defn- accept [^Selector selector]
  (.select selector TIMEOUT)
  (let [selectedKeys (.selectedKeys selector)
        iterator (.iterator selectedKeys)]
    (while (.hasNext iterator)
      (accept-key (.next iterator))
      (.remove iterator)
      )
    )
  (recur selector))

(defn- init-thread-task [selector]
  (reify Runnable (run [_] ((while true (try
                                          (do
                                            (log/info "User message reading thread has been started")
                                            (accept selector))
                                          (catch Exception e (.printStackTrace e))
                                          )))))
  )

(defn- initSelectors [count]
  ;TODO Create thread factory
  (for [_ (range count)]
    (let [executor (Executors/newSingleThreadExecutor)
          selector (Selector/open)]
      (let [runnable (init-thread-task selector)]
        (.execute executor runnable)
        )
      (hash-map :selector selector :executor executor)
      )
    )
  )

(defn -main
  "I don't do a whole lot."
  [& args]
  (if-let [host (first args)]
    (if-let [port (second args)]
      (let [inetAddr (InetSocketAddress. host (Integer/parseInt port))
            serverCh (ServerSocketChannel/open)
            selector (Selector/open)
            socket (.socket serverCh)]
        (.configureBlocking serverCh false)
        (.bind socket inetAddr)
        (.register serverCh selector SelectionKey/OP_ACCEPT)
        (binding [SELECTORS (initSelectors EVENT_LOOP_THREADS)]
          (accept selector))
        )
      (log/error "Port is not specified"))
    (log/error "Host is not specified"))
  )
